import { configService } from './config.service';

module.exports = configService.getTypeOrmConfig();

import {
  BaseEntity,
  Column,
  CreateDateColumn,
  Entity,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { EntityInterface } from '../../interface/entity.interface';
import { IsDate, IsString, IsUUID } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';

@Entity()
export class Post extends BaseEntity implements EntityInterface {
  @ApiProperty()
  @PrimaryGeneratedColumn('uuid')
  @IsUUID('4')
  uuid: string;

  @ApiProperty()
  @Column()
  @IsString()
  title: string;

  @ApiProperty()
  @Column('text')
  @IsString()
  content: string;

  @ApiProperty()
  @CreateDateColumn()
  @IsDate()
  createdAt: Date;
}

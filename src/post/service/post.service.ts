import {
  HttpException,
  HttpStatus,
  Injectable,
  NotFoundException,
} from '@nestjs/common';
import { PostDto } from '../dto/post.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Post } from '../entities/post.entity';
import { PostDataTransformer } from '../dataTransformer/post.data.transformer';

@Injectable()
export class PostService {
  constructor(
    @InjectRepository(Post) private postRepository: Repository<Post>,
    private postDataTransformer: PostDataTransformer,
  ) {}

  async create(postDto: PostDto): Promise<Post> {
    const post = this.postDataTransformer.transform(
      this.postRepository.create(),
      postDto,
    );

    await this.postRepository.save(post);

    return post;
  }

  async findAll(): Promise<Post[]> {
    return await this.postRepository.find();
  }

  async findOne(uuid: string): Promise<Post> {
    const post = await this.postRepository
      .findOne({ where: { uuid: uuid } })
      .catch((exception) => {
        throw new HttpException(
          {
            status: HttpStatus.BAD_REQUEST,
            message: exception.message,
          },
          HttpStatus.BAD_REQUEST,
        );
      });

    if (post) {
      return post;
    }

    throw new NotFoundException(`The Post with uuid: ${uuid} is not found.`);
  }

  async update(uuid: string, postDto: PostDto): Promise<void | Post> {
    return await this.findOne(uuid)
      .then((post) => {
        return this.postDataTransformer.transform(post, postDto);
      })
      .then((post) => {
        return this.postRepository.save(post);
      });
  }

  async remove(uuid: string): Promise<Post[]> {
    await this.findOne(uuid).then((post) => {
      this.postRepository.remove(post);
    });

    return this.findAll();
  }
}

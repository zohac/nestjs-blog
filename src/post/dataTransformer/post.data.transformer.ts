import { Injectable } from '@nestjs/common';
import { PostDto } from '../dto/post.dto';
import { DataTransformer } from '../../interface/data-transformer.interface';
import { Post } from '../entities/post.entity';

@Injectable()
export class PostDataTransformer implements DataTransformer {
  transform(post: Post, postDto: PostDto, context = []): Post {
    if (postDto.title) {
      post.title = postDto.title;
    }
    if (postDto.content) {
      post.content = postDto.content;
    }

    return post;
  }
}

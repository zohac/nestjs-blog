import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Post,
  Put,
} from '@nestjs/common';
import { PostService } from './service/post.service';
import { PostDto } from './dto/post.dto';
import { Post as PostEntity } from './entities/post.entity';
import { ApiCreatedResponse, ApiTags } from '@nestjs/swagger';

@ApiTags('Posts')
@Controller('api/posts')
export class PostController {
  constructor(private readonly postService: PostService) {}

  @Post()
  create(@Body() postDto: PostDto): Promise<PostEntity> {
    return this.postService.create(postDto);
  }

  @Get()
  @ApiCreatedResponse({
    description: 'Get all Posts.',
    type: [PostEntity],
  })
  findAll(): Promise<PostEntity[]> {
    return this.postService.findAll();
  }

  @Get(':uuid')
  @ApiCreatedResponse({
    description: 'Get one Post.',
    type: PostEntity,
  })
  findOne(@Param('uuid') uuid: string): Promise<PostEntity> {
    return this.postService.findOne(uuid);
  }

  @Put(':uuid')
  update(
    @Param('uuid') uuid: string,
    @Body() postDto: PostDto,
  ): Promise<void | PostEntity> {
    return this.postService.update(uuid, postDto);
  }

  @Delete(':uuid')
  remove(@Param('uuid') uuid: string): Promise<PostEntity[]> {
    return this.postService.remove(uuid);
  }
}

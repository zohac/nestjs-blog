import { DtoInterface } from '../../interface/dto.interface';
import { ApiProperty } from '@nestjs/swagger';

export class PostDto implements DtoInterface {
  @ApiProperty()
  readonly title: string;

  @ApiProperty()
  readonly content: string;
}

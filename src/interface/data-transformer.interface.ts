import { EntityInterface } from './entity.interface';
import { DtoInterface } from './dto.interface';

export interface DataTransformer {
  transform(
    entity: EntityInterface,
    entityDto: DtoInterface,
    context: any,
  ): EntityInterface;
}
